# builder

## Build images for EOLE 3 dev/test/run infrastructure

### Depends
You need Packer 1.7.4 or superior.

### How to build

You can use the "build" wrapper, it starts the packer command in the proper order.
Provide the OS and the version like "./build start debian 10".

All the available OS and versions can be listed with :
  $ ./build list

You also can build all the recipes with :
  $ ./build all

### Post-processors
#### onepublish
This post-processor createss a new image on an opennebula datastore.
To make it work you will need the ruby opennebula library installed on the builder.
Then you will need an user and a token with the appropriate permissions.

You can place your builder behind a reverse proxy, but it needs to redirect requests like this :

http://reverse_proxy_addr/whatever/builder_ip/builder_port/image_path

This have to produce a redirection to :

http://builder_ip:builder_port/image_path

For nginx you can use something like this :

```
	location ~* /images/([^/]+)/([^/]+)/(.*) {
		proxy_pass http://$1:$2/$3;
	}
```
