#!/bin/sh

# https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/-/issues/63
cat <<EOF >/etc/sysctl.d/99-k3d.conf
fs.inotify.max_user_instances = 1024
fs.inotify.max_user_watches = 1000000
EOF

apt-get update
apt-get install git -y

# Clone Provisionner
cd /tmp
git clone https://gitlab.mim-libre.fr/EOLE/eole-3/provisionner.git
cd provisionner
git checkout develop
echo "${USER}"
/bin/sh install-eolebase3.sh
