#!bin/sh

CURRENT_DIR=$(pwd)
OPNSENSE_VERSION=21.7

pkg update
pkg install -y git rsync openssl

# Reset local opnsense git repositories
git clone https://github.com/opnsense/tools.git /usr/tools
cd /usr/tools

# Update all or individual repositories (core, plugins, ports, portsref, src, tools)
make update

openssl genrsa -out /usr/tools/config/${OPNSENSE_VERSION}/repo.key 4096
chmod 0400 /usr/tools/config/${OPNSENSE_VERSION}/repo.key
openssl rsa -in /usr/tools/config/${OPNSENSE_VERSION}/repo.key -out /usr/tools/config/${OPNSENSE_VERSION}/repo.pub -pubout

cd /usr/tools

echo "sysutils/py-salt                                arm,arm64" >>/usr/tools/config/${OPNSENSE_VERSION}/ports.conf
sed -e "s/mail\/postfix35.*//g" /usr/tools/config/${OPNSENSE_VERSION}/ports.conf
echo "ADDITIONS=py38-salt" >>/usr/tools/config/${OPNSENSE_VERSION}/build.conf.local

make prefetch-base
make prefetch-kernel
make prefetch-packages
# Cache all distribution files to avoid spurious rebuilds
make distfiles
make ports-py-salt
make dvd
cd $CURRENT_DIR
