#!/bin/sh
set -xeo pipefail

# Run the installer
echo "y" | setup-alpine -e -f install.conf

# Copy ssh keys
echo "Copy packer ssh key"
mount /dev/vg0/lv_root /mnt
cp -rp .ssh /mnt/root/
sync
umount /mnt

echo "Rebooting the host after install"
reboot
