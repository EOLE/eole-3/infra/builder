#!/bin/bash

ONE_CONTEXT_VERSION="6.2.0"
ONE_CONTEXT_PKG_VERSION="1"

cd /tmp || exit 3
wget --no-check-certificate "https://github.com/OpenNebula/addon-context-linux/releases/download/v${ONE_CONTEXT_VERSION}/one-context_${ONE_CONTEXT_VERSION}-${ONE_CONTEXT_PKG_VERSION}.deb"

apt-get purge -y cloud-init
apt-get install -y ./one-context_*deb

# Workaround https://github.com/OpenNebula/addon-context-linux/issues/251
sed -i -e '/^set -e/a export LANG=C.UTF-8' /usr/sbin/one-contextd
