#Flavour grafana
build {
  name = "grafana"
  description = <<EOF
This builder builds a QEMU image from the base build output. The goal here is to install grafana
with it's provisionning.
EOF

  source "source.qemu.alpine" {
    output_directory = "${var.output_dir}/${var.version}/provisionned/grafana"
    vm_name          = "${local.output_name}-${var.version}-grafana.img"
    iso_url          = "${var.output_dir}/${var.version}/base/${local.output_name}-${var.version}.img"
    iso_checksum     = "none"
    disk_size        = 40960
    disk_image       = true
    boot_command     = [ "<enter><enter><wait>" ]
    ssh_clear_authorized_keys = true
  }

  provisioner "file" {
    destination = "/tmp/${build.name}.sh"
    source      = "${path.cwd}/provisionning/${var.name}/${build.name}.sh"
  }

  provisioner "file" {
    destination = "/etc/conf.d/grafana"
    source      = "${path.cwd}/provisionning/${var.name}/conf/${build.name}/${build.name}.conf"
  }

  provisioner "file" {
    destination = "/tmp/one-context.sh"
    source      = "${path.cwd}/provisionning/${var.name}/one-context.sh"
  }

  provisioner "shell" {
    inline = [
      "sh -cx 'sh /tmp/one-context.sh'",
      "sh -cx 'sh /tmp/${build.name}.sh'"
    ]
  }

  post-processor "shell-local" {
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${var.output_dir}/${var.version}/provisionned/grafana ${var.image_version}",
      "ruby ${path.cwd}/tools/one-templates -t image -T ${path.cwd}/templates/one/image/common.tpl -n ${local.output_name}-${var.version}-grafana -c 'grafana base image' --image-file ${var.output_dir}/${var.version}/provisionned/grafana/${local.output_name}-${var.version}-grafana.img",
      "ruby ${path.cwd}/tools/one-templates -t vm -T ${path.cwd}/templates/one/vm/common.xml -n ${local.output_name}-${var.version}-grafana --image-name ${local.output_name}-${var.version}-grafana",
    ]
  }

}
