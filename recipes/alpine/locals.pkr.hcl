# "timestamp" template function replacement
locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
  output_name = "${var.name}"
  source_checksum_url = "file:${var.source_url}/${var.version}/${var.arch}/iso-cd/SHA256SUMS"
  source_iso = "${var.source_url}/v${var.short_version}/releases/${var.arch}/alpine-virt-${var.version}-${var.arch}.iso"
  source_checksum = "${var.iso_cd_checksum}"
  ssh_user = "root"
  ssh_password = "PbkRc1vup7Wq5n4r"
  disk_size = 8000
  memory = 512
  installOpts = {
    hostname = var.name
    user = "eole"
  }
}
