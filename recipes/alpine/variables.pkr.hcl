variable "name" {
  type = string
  default = "alpine"
}

variable "version" {
  type = string
  default = "3.14.2"
}

variable "short_version" {
  type = string
  default = "3.14"
}

variable "arch" {
  type = string
  default = "x86_64"
}

variable "output_dir" {
  type    = string
  default = "output/alpine/"
}

variable "source_url" {
  type = string
  default = "https://cdimage.debian.org/cdimage/release"
}

variable "iso_cd_checksum" {
  type = string
  default = "sha256:ae6d563d2444665316901fe7091059ac34b8f67ba30f9159f7cef7d2fdc5bf8a"
}

variable "image_version" {
  type = string
  default = "0.0.1"
}

variable "one_user" {
  type = string
  default = env("ONE_USER")
}

variable "one_token" {
  type = string
  default = env("ONE_TOKEN")
}

variable "boot_command" {
  type = list(string)
  default = []
}
