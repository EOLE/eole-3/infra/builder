#Flavour k3s
build {
  name = "k3s"
  description = <<EOF
This builder builds a QEMU image from the base build output. The goal here is to install k3s
with it's provisionning.
EOF

  source "source.qemu.alpine" {
    output_directory = "${var.output_dir}/${var.version}/provisionned/k3s"
    vm_name          = "${local.output_name}-${var.version}-k3s.img"
    iso_url          = "${var.output_dir}/${var.version}/base/${local.output_name}-${var.version}.img"
    iso_checksum     = "none"
    disk_size        = 40960
    disk_image       = true
    boot_command     = [ "<enter><enter><wait>" ]
    ssh_clear_authorized_keys = true
  }

  provisioner "file" {
    destination = "/tmp/${build.name}.sh"
    source      = "${path.cwd}/provisionning/${var.name}/${build.name}.sh"
  }

  provisioner "file" {
    destination = "/tmp/one-context.sh"
    source      = "${path.cwd}/provisionning/${var.name}/one-context.sh"
  }

  provisioner "shell" {
    inline = [
      "sh -cx 'sh /tmp/one-context.sh'",
      "sh -cx 'sh /tmp/${build.name}.sh'"
    ]
  }

  post-processor "shell-local" {
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${var.output_dir}/${var.version}/provisionned/k3s ${var.image_version}",
      "ruby ${path.cwd}/tools/one-templates -t image -T ${path.cwd}/templates/one/image/common.tpl -n ${local.output_name}-${var.version}-k3s -c 'K3S base image' --image-file ${var.output_dir}/${var.version}/provisionned/k3s/${local.output_name}-${var.version}-k3s.img",
      "ruby ${path.cwd}/tools/one-templates -t vm -T ${path.cwd}/templates/one/vm/k3s.xml -n ${local.output_name}-${var.version}-k3s --image-name ${local.output_name}-${var.version}-k3s",
      "ruby ${path.cwd}/tools/one-templates -t service -T ${path.cwd}/templates/one/service/k3s-cluster.json -n k3s-cluster-${var.version} --vm-name ${local.output_name}-${var.version}-k3s",
      "ruby ${path.cwd}/tools/one-templates -t service -T ${path.cwd}/templates/one/service/k3s-revprox.json -n k3s-cluster-revprox-${var.version} --vm-name ${local.output_name}-${var.version}-k3s"
    ]
  }

}
