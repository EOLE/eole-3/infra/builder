variable "name" {
  type = string
  default = "debian"
}

variable "version" {
  type = string
  default = "9.10.0"
}

variable "short_version" {
  type = string
  default = "9"
}

variable "arch" {
  type = string
  default = "amd63"
}

variable "output_dir" {
  type    = string
  default = "output/freebsd/"
}

variable "source_url" {
  type = string
  default = "http://ftp.freebsd.org/pub/FreeBSD/releases/ISO-IMAGES"
}

variable "source_iso_name" {
  type = string
  default = "FreeBSD-13.0-RELEASE-amd64-disc1.iso"
}

variable "iso_cd_checksum" {
  type = string
  default = "sha256:ae6d563d2444665316901fe7091059ac34b8f67ba30f9159f7cef7d2fdc5bf8a"
}

variable "boot_command" {
  type = list(string)
  default = []
}

variable "image_version" {
  type = string
  default = "0.0.1"
}