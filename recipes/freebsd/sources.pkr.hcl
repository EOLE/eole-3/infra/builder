source qemu "freebsd" {
  cpus = 2
  memory      = 8192
  accelerator = "kvm"

  headless = true

  # Serve the `http` directory via HTTP, used for preseeding the Debian installer.
  http_directory = "${path.cwd}/provisionning/${var.name}/http"
  http_port_min  = 9990
  http_port_max  = 9999

  # SSH ports to redirect to the VM being built
  host_port_min = 2222
  host_port_max = 2229
  # This user is configured in the preseed file.
  ssh_password     = "${local.ssh_password}"
  ssh_username     = "${local.ssh_user}"
  ssh_wait_timeout = "1000s"

  shutdown_command = "/sbin/poweroff"

  # Builds a compact image
  disk_compression   = false
  disk_discard       = "unmap"
  skip_compaction    = true
  disk_detect_zeroes = "unmap"

  format           = "qcow2"

  boot_wait = "1s"
}