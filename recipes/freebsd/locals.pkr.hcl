# "timestamp" template function replacement
locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
  output_name = "${var.name}"
  source_checksum_url = "file:${var.source_url}/${var.version}/${var.arch}/iso-cd/SHA256SUMS"
  #source_iso = "${var.source_url}/${var.version}-RELEASE/${var.arch}/Latest/FreeBSD-${var.version}-RELEASE-${var.arch}.qcow2.xz"
  source_iso = "${var.source_url}/${var.version}/${var.source_iso_name}"
  source_checksum = "${var.iso_cd_checksum}"
  ssh_user = "root"
  ssh_password = "packer"
}
