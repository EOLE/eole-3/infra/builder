# "timestamp" template function replacement
locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
  output_directory = "output/eole2"
  output_name = "${var.name}"
  source_iso = "${var.source_url}/${var.code_name}/${var.image_dir_name}/${var.code_name}-server-cloudimg-${var.arch}.img"
  source_checksum = "file:${var.source_url}/${var.code_name}/current/SHA256SUMS"
  ssh_user = "packer"
  ssh_password = "packer"
  instance_data = {
     "instance-id": "${var.name}"
  }
}