build {
  name = "base"
  description = <<EOF
This builder builds a QEMU image from a Debian "netinst" CD ISO file.
EOF

  source "qemu.eole2" {
    output_directory = "${local.output_directory}/${var.eole_release}/base"
    vm_name          = "${local.output_name}-${var.eole_release}.img"
    boot_command     = var.boot_command
    iso_url      = "${local.source_iso}"
    iso_checksum = "${local.source_checksum}"
    disk_size   = 8000
    disk_image       = true
    cd_content = {
      "meta-data" = jsonencode(local.instance_data)
      "user-data" = templatefile("${path.cwd}/templates/conf/cloud-init/user-data",
        { user = local.ssh_user,
          password = local.ssh_password,
          runcmd = var.cloud_init_runcmd })
    }
    cd_label =  "cidata"
  }

  provisioner "ansible" {
    playbook_file = "${path.cwd}/provisionning/${var.name}/playbooks/tmate.yaml"
    ansible_env_vars = [
      "ANSIBLE_SSH_ARGS='-oHostKeyAlgorithms=+ssh-rsa -oPubkeyAcceptedKeyTypes=ssh-rsa'",
      "ANSIBLE_HOST_KEY_CHECKING=False"
    ]
    user="${local.ssh_user}"
    extra_arguments = [
      "--scp-extra-args", "'-O'",
      "--extra-vars", "VM_NAME=${local.output_name}",
      "--extra-vars", "HTTP_PROXY=${var.HTTP_PROXY} HTTPS_PROXY=${var.HTTPS_PROXY}",
      "--extra-vars", "http_proxy=${var.http_proxy} https_proxy=${var.https_proxy}",
      "--extra-vars", "envole_version=${var.envole_version}",
      "--extra-vars", "eole_release=${var.eole_release}"
    ]
  }

  provisioner "ansible" {
    playbook_file = "${path.cwd}/provisionning/${var.name}/playbooks/${var.name}.yaml"
    ansible_env_vars = [
      "ANSIBLE_SSH_ARGS='-oHostKeyAlgorithms=+ssh-rsa -oPubkeyAcceptedKeyTypes=ssh-rsa'",
      "ANSIBLE_HOST_KEY_CHECKING=False"
    ]
    user="${local.ssh_user}"
    extra_arguments = [
      "--scp-extra-args", "'-O'",
      "--extra-vars", "VM_NAME=${local.output_name}",
      "--extra-vars", "HTTP_PROXY=${var.HTTP_PROXY} HTTPS_PROXY=${var.HTTPS_PROXY}",
      "--extra-vars", "http_proxy=${var.http_proxy} https_proxy=${var.https_proxy}",
      "--extra-vars", "eole_release=${var.eole_release}"
    ]
  }

  post-processor "shell-local" {
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${local.output_directory}/${var.eole_release}/base ${var.image_version}",
    ]
  }

  post-processor "manifest" {
    keep_input_artifact = true
  }
}