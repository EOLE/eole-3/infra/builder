source qemu "debian" {
  cpus = 2
  memory      = local.memory
  accelerator = "kvm"

  headless = true

  # SSH ports to redirect to the VM being built
  host_port_min = 2222
  host_port_max = 2229
  # This user is configured in the preseed file.
  ssh_password     = "${local.ssh_password}"
  ssh_username     = "${local.ssh_user}"
  ssh_wait_timeout = "5m"

  net_device = "virtio-net-pci"

  shutdown_command = "echo '${local.ssh_password}'  | sudo -S /sbin/shutdown -hP now"

  # Builds a compact image
  disk_compression   = false
  disk_discard       = "unmap"
  skip_compaction    = true
  disk_detect_zeroes = "unmap"

  format             = "qcow2"

  boot_wait = "2s"
}
