# "timestamp" template function replacement
locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
  output_directory = "output/debian"
  output_name = "${var.name}"
  source_checksum_url = "file:${var.source_url}/SHA512SUMS"
  source_iso = "${var.source_url}/debian-${var.version}-generic-${var.arch}.qcow2"
  ssh_user = "packer"
  ssh_password = "packer"
  memory = 2048
  instance_data = {
     "instance-id": "${var.name}"
  }
}