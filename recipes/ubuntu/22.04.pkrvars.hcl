name = "ubuntu"
vm_hostname = "ubt2204"
version = "22.04.3"
short_version = "22.04"
code_name = "jammy"
arch = "amd64"
source_url = "https://cloud-images.ubuntu.com"
image_dir_name = "current"
boot_command = [ "<enter>" ]
#cloud_init_runcmd = [ "dhclient ens3" ]
